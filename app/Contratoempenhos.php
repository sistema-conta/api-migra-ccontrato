<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratoempenhos extends Model
{
    protected $table = 'contratoempenhos';
    protected $primaryKey = 'id';
}
