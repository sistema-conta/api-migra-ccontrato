<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fatura extends Model
{
    protected $table = 'fatura';

    protected $primaryKey = 'fat_id';

    public function getFatTliIdAttribute($value)
    {
        $descricao = $this->buscaTipoLista($value);
        return $descricao;
    }

    public function getFatJusIdAttribute($value)
    {
        $descricao = $this->buscaJustificativa($value);
        return $descricao;
    }


    private function buscaTipoLista($id)
    {
        $retorno = Tipolista::find($id);
        return $retorno->tli_nome;
    }

    private function buscaJustificativa($id)
    {
        $retorno = Justificativa::find($id);

        if(!isset($retorno->jus_id)){
            return '';
        }

        return $retorno->jus_nome;
    }


    public function empenhos()
    {
        return $this->belongsToMany(Empenho::class, 'faturaxempenho', 'fxe_fat_id', 'fxe_emp_id');
    }


}
