<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipolista extends Model
{
    protected $table = 'tipolista';

    protected $primaryKey = 'tli_id';
}
