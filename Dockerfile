#
# Script base para Aplicações Laravel
#

#
# Container temporário para instalar as dependências via Composer
# 
FROM composer:1.9.2 as composer
WORKDIR /app
COPY composer.json composer.json
RUN composer --no-ansi install --no-ansi --prefer-dist --no-dev --no-scripts --no-interaction --ignore-platform-reqs --no-autoloader

# 
# Container da Aplicação
#
FROM php:7.4.2-fpm-alpine3.11

WORKDIR /opt

RUN apk add -Uuv \
    bash \
    vim \
    openrc \
    oniguruma-dev \
    libxml2-dev \
    autoconf \
    gcc \
    g++ \
    make \
    unixodbc-dev \
    gnupg \
    postgresql-dev \
    && rm -rf /var/cache/apk/*

# Instalar dependências do PHP para o Laravel
# RUN NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1)
# RUN docker-php-ext-install "-j${NPROC}" \
RUN docker-php-ext-install \
    bcmath  \
    ctype  \
    json \
    mbstring \
    pdo \
    tokenizer \
    xml

# Instalar dependências do banco de dados
RUN docker-php-ext-install \
    pdo_pgsql

# Install wait-for-it
RUN curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh > /opt/wait-for-it.sh \
    && chmod +x /opt/wait-for-it.sh \
    && ln -s /opt/wait-for-it.sh /usr/bin/wait-for-it

# Instalar composer
ARG COMPOSER_COMMIT_HASH=b9cc694e39b669376d7a033fb348324b945bce05
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/${COMPOSER_COMMIT_HASH}/web/installer -O - -q | php -- --quiet
RUN chmod +x /opt/composer.phar \
    && ln -s /opt/composer.phar /usr/bin/composer

# Define Workdir
WORKDIR /app

# Copy application from composer temporary container
COPY --from=composer /app /app

# Copy source to container
COPY . /app

# Create composer autoloader
RUN composer --no-ansi dump-autoload --no-scripts --no-dev --no-interaction --optimize

# Ports Exposed
EXPOSE 9000

# Set docker scripts to $path
ENV PATH $PATH:/app/docker/entrypoint

# Copy status.sh to correct path for kubernetes' monitor
COPY ./docker/entrypoint/status.sh /monitoramento/

# Set permissions (bash files)
RUN chmod +x ./docker/entrypoint/*
RUN chmod +x /monitoramento/*

# Run start script
ENTRYPOINT ["entrypoint.sh"]

# Start php-fpm
CMD ["php-fpm"]